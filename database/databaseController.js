import Sequelize from 'sequelize';

const sequelize = new Sequelize('database', null, null, {
  dialect: 'sqlite',
  storage: './database.sqlite',
});

// ********* MODELS **********

const Movie = sequelize.define('movie', {
  Title: {
    type: Sequelize.STRING,
    unique: true,
  },
  Year: Sequelize.INTEGER,
  Rated: Sequelize.STRING,
  Released: Sequelize.STRING,
  Runtime: Sequelize.STRING,
  Genre: Sequelize.STRING,
  Director: Sequelize.STRING,
  Writer: Sequelize.STRING,
  Actors: Sequelize.STRING,
  Plot: Sequelize.STRING,
  Language: Sequelize.STRING,
  Country: Sequelize.STRING,
  Awards: Sequelize.STRING,
  Ratings: Sequelize.STRING,
  Metascore: Sequelize.STRING,
  imdbRating: Sequelize.STRING,
  imdbVotes: Sequelize.STRING,
  imdbID: Sequelize.STRING,
  Type: Sequelize.STRING,
  DVD: Sequelize.STRING,
  BoxOffice: Sequelize.STRING,
  Production: Sequelize.STRING,
  Website: Sequelize.STRING,
  Response: Sequelize.STRING,
});

Movie.sync({ force: true });

const Comment = sequelize.define('comment', {
  MovieID: {
    type: Sequelize.INTEGER,
  },
  Comment: {
    type: Sequelize.STRING,
  },
});

Comment.sync({ force: true });

Movie.hasMany(Comment, { foreignKey: 'MovieID' });
Comment.belongsTo(Movie, {
  foreignKey: 'MovieID',
  onDelete: 'cascade',
});


// ********* FUNCTIONS **********

function checkConnection() {
  return new Promise(((resolve, reject) => {
    sequelize
      .authenticate()
      .then(() => {
        resolve(true);
      }, (err) => {
        reject(err);
      });
  }));
}

function postMovie(movieObject) {
  const movieObjectNewRatings = movieObject;
  movieObjectNewRatings.Ratings = JSON.stringify(movieObjectNewRatings.Ratings);
  return new Promise(((resolve, reject) => {
    Movie.create(movieObjectNewRatings)
      .then((response) => {
        console.log(`databaseController postMovie: ${response}`);
        resolve(response);
      })
      .catch((err) => {
        console.log(`databaseController postMovie ERROR: ${JSON.stringify(err)}`);
        if (err.name === 'SequelizeUniqueConstraintError') {
          reject(new Error(`Movie already exist in database (${err.name})`));
        } else {
          reject(err.name);
        }
      });
  }));
}

function getMovies() {
  return new Promise((resolve, reject) => {
    Movie.findAll()
      .then((response) => {
        resolve(response);
      })
      .catch((err) => {
        console.log(`databaseController getMovies ERROR: ${err}`);
        reject(err);
      });
  });
}

// TODO - Rework it to only one request (without if statement)
function getComments(MovieIDReq) {
  if (MovieIDReq.MovieID !== undefined) {
    return new Promise(((resolve, reject) => {
      Comment.findAll({
        where: MovieIDReq,
      })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          console.log(`databaseController getComments ERROR: ${err}`);
          reject(err);
        });
    }));
  }
  return new Promise(((resolve, reject) => {
    Comment.findAll()
      .then((response) => {
        resolve(response);
      })
      .catch((err) => {
        console.log(`databaseController getComments ERROR: ${err}`);
        reject(err);
      });
  }));
}

function postComment(MovieID, CommentBody) {
  return new Promise(((resolve, reject) => {
    Comment.create({ MovieID, Comment: CommentBody })
      .then((response) => {
        console.log(`databaseController postComment Create res: ${response}`);
        resolve(response);
      })
      .catch((err) => {
        console.log(`databaseController postComment Create ERROR: ${JSON.stringify(err)}`);
        if (err.original.errno === 19) {
          reject(new Error(`Movie does not exist (${err.name})`));
        } else {
          reject(err.name);
        }
      });
  }));
}

export default {
  checkConnection,
  postMovie,
  getMovies,
  getComments,
  postComment,
};
