/* global define, it, describe, done */
import chai from 'chai';
import chaiHttp from 'chai-http';
import serverApp from '../index';

// eslint-disable-next-line no-unused-vars
const should = chai.should();

chai.use(chaiHttp);
setTimeout(() => {
  describe('Movies POST', () => {
    it('should post movie object in database', (done) => {
      const movieObject = {
        title: 'Star Wars',
      };
      chai.request(serverApp)
        .post('/movies')
        .send(movieObject)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
    });
    it('should NOT post a movie (request without body)', (done) => {
      chai.request(serverApp)
        .post('/movies')
        .send({})
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });

  describe('Movies GET', () => {
    it('should get all movies from database', (done) => {
      chai.request(serverApp)
        .get('/movies')
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  });

  describe('Comments POST', () => {
    it('should post comment at specific movie', (done) => {
      const commentObject = {
        movieID: 1,
        comment: 'Test comment',
      };
      chai.request(serverApp)
        .post('/comments')
        .send(commentObject)
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });

    it('should NOT post comment at non existing movie', (done) => {
      const commentObject = {
        movieID: 123,
        comment: 'Test comment',
      };
      chai.request(serverApp)
        .post('/comments')
        .send(commentObject)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });

  describe('Comment GET', () => {
    it('should get all comments', (done) => {
      chai.request(serverApp)
        .get('/comments')
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
    it('should get comments from specific movie', (done) => {
      chai.request(serverApp)
        .get('/comments/1')
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
    it('should NOT get comment from non existing movie', (done) => {
      chai.request(serverApp)
        .get('/comments/123')
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });
  run();
}, 2000);
