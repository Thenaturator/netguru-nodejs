import fetchManager from '../../fetchManager/fetchManager';
import databaseController from '../../database/databaseController';

function getMovies(req, res) {
  databaseController.getMovies()
    .then((allMovies) => {
      if (allMovies.length === 0) {
        res.status(400);
        res.send('There is no movies');
      } else {
        res.json(allMovies);
      }
    })
    .catch((err) => {
      console.log(`getMovies ERROR: ${err}`);
      res.status(400);
      res.send(err);
    });
}

function postMovies(req, res) {
  if (typeof req.body.title !== 'undefined' && req.body.title.length > 0) {
    fetchManager.getMovieByTitleOMDB(req.body.title)
      .then((movieObjectOMDB) => {
        if (movieObjectOMDB.Response === 'True') {
          databaseController.postMovie(movieObjectOMDB)
            .then((postMovieRes) => {
              res.json(postMovieRes);
            })
            .catch((err) => {
              res.status(400);
              res.json(err.message);
            });
        } else {
          res.status(400);
          res.json(movieObjectOMDB); // Send error from external request
        }
      })
      .catch((err) => {
        console.log(`postMovie ERROR: ${err}`);
        res.status(400);
        res.send(err);
      });
  } else {
    res.status(400);
    res.send('Movie title is not given.');
  }
}

function getComments(req, res) {
  console.log(`getComments req.param: ${JSON.stringify(req.params)}`);
  databaseController.getComments(req.params)
    .then((allComments) => {
      if (allComments.length === 0) {
        res.status(400);
        res.send('There are no comments');
      } else {
        res.json(allComments);
      }
    })
    .catch((err) => {
      console.log(`getComments ERROR: ${err}`);
      res.status(400);
      res.send(err);
    });
}

function postComments(req, res) {
  console.log(`postComments req: ${JSON.stringify(req.body)}`);
  databaseController.postComment(req.body.movieID, req.body.comment)
    .then((postCommentRes) => {
      console.log(`postComment: ${postCommentRes}`);
      res.json(postCommentRes);
    })
    .catch((err) => {
      console.log(`postComment ERROR: ${err}`);
      res.status(400);
      res.send(JSON.stringify(err.message));
    });
}

export default {
  getMovies,
  postMovies,
  getComments,
  postComments,
};
