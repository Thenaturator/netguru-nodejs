import moviesCommentsController from '../controllers/moviesCommentsController';

export default (app) => {
  app.route('/movies')
    .get(moviesCommentsController.getMovies)
    .post(moviesCommentsController.postMovies);

  app.route('/comments/:MovieID?')
    .get(moviesCommentsController.getComments)
    .post(moviesCommentsController.postComments);
};
