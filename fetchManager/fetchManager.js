import axios from 'axios';
import util from 'util';
import config from '../config.json';

function getMovieByTitleOMDB(title) {
  return new Promise((resolve, reject) => {
    axios.get(`http://www.omdbapi.com/?t=${title}&apikey=${config.apiKey}`)
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        console.log(`getMovieByTitle ERROR res: ${util.inspect(err)}`);
        reject(err);
      });
  });
}

export default { getMovieByTitleOMDB };
