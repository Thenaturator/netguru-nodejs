# Netguru - NodeJS task

## Instalation

* Clone/Download Repo
* Run ``npm install`` command
* Create ``config.json`` file (example is provided)
* Run server with ``npm run start`` command
* Run tests with ``npm run test`` command

> Default port is ``3000``

### Requirements 

* NodeJS
* NPM

## Used Libraries

#### ExpressJS

For me is a basic library to create web application on NodeJS. Well documented and easy to use.

#### Sequelize

Also well documented library. Easy to use with every database. Easy to change to another one

##### SQLite

Used only for practicality, easy to implement.

#### Axios

Most important feature is Promise based requests. But also well documented and easy to use.

#### ESlint with Airbnb preset

Improve quality of code and provide standardization of it.

#### Mocha / Chai

Easy to use test framework.

#### Babel with Latest preset

JavaScript compiler