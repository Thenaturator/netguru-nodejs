import express from 'express';
import bodyParser from 'body-parser';
import moviesCommentsRoutes from './api/routes/moviesCommentsRoutes';

const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.listen(port);

moviesCommentsRoutes(app);

console.log(`RESTful API server started on: ${port}`);

export default app;
